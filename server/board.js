exports.default = [
	{name: 'PIC_0-0-0', cat: 0, lin: 0, col: 0, txt: ['Objet', 'Paquet', 'Carton']},
	{name: 'PIC_0-0-1', cat: 0, lin: 0, col: 1, txt: ['Famille', 'Société', 'Groupe']},
	{name: 'PIC_0-1-0', cat: 0, lin: 1, col: 0, txt: ['Femme', 'Féminin', 'Femelle']},
	{name: 'PIC_0-1-1', cat: 0, lin: 1, col: 1, txt: ['Homme', 'Masculin', 'Mâle']},
	{name: 'PIC_0-2-0', cat: 0, lin: 2, col: 0, txt: ['Travail', 'Métier']},
	{name: 'PIC_0-2-1', cat: 0, lin: 2, col: 1, txt: ['Loisir', 'Sport', 'Activité']},
	{name: 'PIC_0-3-0', cat: 0, lin: 3, col: 0, txt: ['Faune', 'Animal']},
	{name: 'PIC_0-3-1', cat: 0, lin: 3, col: 1, txt: ['Flore', 'Plante', 'Nature']},
	{name: 'PIC_0-4-0', cat: 0, lin: 4, col: 0, txt: ['Littérature', 'Écrivain', 'Livre']},
	{name: 'PIC_0-4-1', cat: 0, lin: 4, col: 1, txt: ['Musique', 'Chanson', 'Note']},
	{name: 'PIC_0-5-0', cat: 0, lin: 5, col: 0, txt: ['Cinéma', 'Film', 'Caméra']},
	{name: 'PIC_0-5-1', cat: 0, lin: 5, col: 1, txt: ['Arts', 'Sculpture', 'Dessin']},
	{name: 'PIC_0-6-0', cat: 0, lin: 6, col: 0, txt: ['Télévision', 'Émission', 'Série']},
	{name: 'PIC_0-6-1', cat: 0, lin: 6, col: 1, txt: ['Titre', 'Marque', 'Nom']},
	{name: 'PIC_0-7-0', cat: 0, lin: 7, col: 0, txt: ['Idée', 'Pensée', 'Concept']},
	{name: 'PIC_0-7-1', cat: 0, lin: 7, col: 1, txt: ['Expression', 'Langue', 'Dialogue', 'Bande-dessinée']},
	{name: 'PIC_0-8-0', cat: 0, lin: 8, col: 0, txt: ['Lieu', 'Pays', 'Drapeau']},
	{name: 'PIC_0-8-1', cat: 0, lin: 8, col: 1, txt: ['Bâtiment', 'Construction', 'Ville']},
	{name: 'PIC_0-9-0', cat: 0, lin: 9, col: 0, txt: ['Date', 'Événement', 'Jour']},
	{name: 'PIC_0-9-1', cat: 0, lin: 9, col: 1, txt: ['Fétes', 'Anniversaires']},
	{name: 'PIC_0-10-0', cat: 0, lin: 10, col: 0, txt: ['Véhicule marin', 'Maritime', 'Nager']},
	{name: 'PIC_0-10-1', cat: 0, lin: 10, col: 1, txt: ['Véhicule volant', 'Aérien', 'Voler']},
	{name: 'PIC_0-11-0', cat: 0, lin: 11, col: 0, txt: ['Véhicule routier', 'Voiture', 'Rouler']},
	{name: 'PIC_0-11-1', cat: 0, lin: 11, col: 1, txt: ['Outils']},
	{name: 'PIC_0-12-0', cat: 0, lin: 12, col: 0, txt: ['Jeux', 'Jouet']},
	{name: 'PIC_0-12-1', cat: 0, lin: 12, col: 1, txt: ['Vétements', 'Accessoires', 'Costume']},
	{name: 'PIC_0-13-0', cat: 0, lin: 13, col: 0, txt: ['Alimentaire', 'Nourriture', 'Comestible']},
	{name: 'PIC_0-13-1', cat: 0, lin: 13, col: 1, txt: ['Maison', 'Intérieur', 'Domestique']},
	{name: 'PIC_1-0-0', cat: 1, lin: 0, col: 0, txt: ['Réel', 'Historique']},
	{name: 'PIC_1-0-1', cat: 1, lin: 0, col: 1, txt: ['Fictif', 'Imaginaire']},
	{name: 'PIC_1-1-0', cat: 1, lin: 1, col: 0, txt: ['Jeune', 'Nouveau', 'Enfant', 'Bébé']},
	{name: 'PIC_1-1-1', cat: 1, lin: 1, col: 1, txt: ['Vieux', 'Ancien', 'Passé']},
	{name: 'PIC_1-2-0', cat: 1, lin: 2, col: 0, txt: ['Lent', 'Tortue']},
	{name: 'PIC_1-2-1', cat: 1, lin: 2, col: 1, txt: ['Rapide', 'Vif', 'Lièvre']},
	{name: 'PIC_1-3-0', cat: 1, lin: 3, col: 0, txt: ['Défense', 'Protection', 'Mur']},
	{name: 'PIC_1-3-1', cat: 1, lin: 3, col: 1, txt: ['Conflit', 'Arme', 'Combattre']},
	{name: 'PIC_1-4-0', cat: 1, lin: 4, col: 0, txt: ['Vie', 'Coeur', 'Amour']},
	{name: 'PIC_1-4-1', cat: 1, lin: 4, col: 1, txt: ['Mort', 'Mal', 'Maladie']},
	{name: 'PIC_1-5-0', cat: 1, lin: 5, col: 0, txt: ['Joyeux', 'Positif']},
	{name: 'PIC_1-5-1', cat: 1, lin: 5, col: 1, txt: ['Triste', 'Négatif']},
	{name: 'PIC_1-6-0', cat: 1, lin: 6, col: 0, txt: ['électronique', 'Informatique']},
	{name: 'PIC_1-6-1', cat: 1, lin: 6, col: 1, txt: ['Mécanique', 'Industriel']},
	{name: 'PIC_1-7-0', cat: 1, lin: 7, col: 0, txt: ['Argent', 'Riche', 'Cher']},
	{name: 'PIC_1-7-1', cat: 1, lin: 7, col: 1, txt: ['Temps', 'Durée']},
	{name: 'PIC_1-8-0', cat: 1, lin: 8, col: 0, txt: ['Religion', 'Mythe', 'Croyance']},
	{name: 'PIC_1-8-1', cat: 1, lin: 8, col: 1, txt: ['Pouvoir', 'Politique']},
	{name: 'PIC_1-9-0', cat: 1, lin: 9, col: 0, txt: ['Sciences', 'Mathématiques', 'Chimie']},
	{name: 'PIC_1-9-1', cat: 1, lin: 9, col: 1, txt: ['Médical', 'Médicament', 'Soigner']},
	{name: 'PIC_1-10-0', cat: 1, lin: 10, col: 0, txt: ['Téte', 'Visage']},
	{name: 'PIC_1-10-1', cat: 1, lin: 10, col: 1, txt: ['Bras', 'Main']},
	{name: 'PIC_1-11-0', cat: 1, lin: 11, col: 0, txt: ['Torse', 'Ventre', 'Corps']},
	{name: 'PIC_1-11-1', cat: 1, lin: 11, col: 1, txt: ['Jambe', 'Pied']},
	{name: 'PIC_1-12-0', cat: 1, lin: 12, col: 0, txt: ['Oreille', 'Son', 'Entendre']},
	{name: 'PIC_1-12-1', cat: 1, lin: 12, col: 1, txt: ['Nez', 'Odeur', 'Sentir']},
	{name: 'PIC_1-13-0', cat: 1, lin: 13, col: 0, txt: ['Oeil', 'Vue', 'Voir']},
	{name: 'PIC_1-13-1', cat: 1, lin: 13, col: 1, txt: ['Bouche', 'Goût', 'Manger']},
	{name: 'PIC_2-0-0', cat: 2, lin: 0, col: 0, txt: ['Nuage', 'Pluie', 'Neige', 'Froid']},
	{name: 'PIC_2-0-1', cat: 2, lin: 0, col: 1, txt: ['Éclair', 'Électricité', 'Tempête', 'Colère']},
	{name: 'PIC_2-1-0', cat: 2, lin: 1, col: 0, txt: ['Nuit', 'Soir', 'Lune']},
	{name: 'PIC_2-1-1', cat: 2, lin: 1, col: 1, txt: ['Soleil', 'Chaud', 'Lumiére', 'Jour']},
	{name: 'PIC_2-2-0', cat: 2, lin: 2, col: 0, txt: ['Feu', 'Bréler']},
	{name: 'PIC_2-2-1', cat: 2, lin: 2, col: 1, txt: ['Eau', 'Liquide', 'Aquatique']},
	{name: 'PIC_2-3-0', cat: 2, lin: 3, col: 0, txt: ['Air', 'Vent', 'Souffler']},
	{name: 'PIC_2-3-1', cat: 2, lin: 3, col: 1, txt: ['Terre']},
	{name: 'PIC_2-4-0', cat: 2, lin: 4, col: 0, txt: ['Roche', 'Minéral', 'Dur']},
	{name: 'PIC_2-4-1', cat: 2, lin: 4, col: 1, txt: ['Bois']},
	{name: 'PIC_2-5-0', cat: 2, lin: 5, col: 0, txt: ['Métal']},
	{name: 'PIC_2-5-1', cat: 2, lin: 5, col: 1, txt: ['Tissus']},
	{name: 'PIC_2-6-0', cat: 2, lin: 6, col: 0, txt: ['Plastique', 'Caoutchouc']},
	{name: 'PIC_2-6-1', cat: 2, lin: 6, col: 1, txt: ['Papier', 'Feuille']},
	{name: 'PIC_2-7-0', cat: 2, lin: 7, col: 0, txt: ['Opposé', 'Contraire', 'Inverse']},
	{name: 'PIC_2-7-1', cat: 2, lin: 7, col: 1, txt: ['Couper', 'Séparer', 'Moitié']},
	{name: 'PIC_2-8-0', cat: 2, lin: 8, col: 0, txt: ['Fragment', 'Multitude', 'Poudre']},
	{name: 'PIC_2-8-1', cat: 2, lin: 8, col: 1, txt: ['Partie de', 'Morceau de', 'Pièce de']},
	{name: 'PIC_2-9-0', cat: 2, lin: 9, col: 0, txt: ['Dedans', 'Interne']},
	{name: 'PIC_2-9-1', cat: 2, lin: 9, col: 1, txt: ['Grille', 'Réseau', 'Prison']},
	{name: 'PIC_2-10-0', cat: 2, lin: 10, col: 0, txt: ['Zéro', 'Néant', 'Nul']},
	{name: 'PIC_2-10-1', cat: 2, lin: 10, col: 1, txt: ['Unité', 'Un']},
	{name: 'PIC_3-0-0', cat: 3, lin: 0, col: 0, txt: ['Ligne droite', 'Droit']},
	{name: 'PIC_3-0-1', cat: 3, lin: 0, col: 1, txt: ['Arc', 'Courbe', 'Bombé']},
	{name: 'PIC_3-1-0', cat: 3, lin: 1, col: 0, txt: ['Croix', 'Croisement']},
	{name: 'PIC_3-1-1', cat: 3, lin: 1, col: 1, txt: ['Ligne brisée', 'Pointu', 'Accidenté']},
	{name: 'PIC_3-2-0', cat: 3, lin: 2, col: 0, txt: ['Spirale', 'Ivresse', 'Folie', 'Poil']},
	{name: 'PIC_3-2-1', cat: 3, lin: 2, col: 1, txt: ['Sinusoédale', 'Ondulation', 'Cheveux', 'Rivière']},
	{name: 'PIC_3-3-0', cat: 3, lin: 3, col: 0, txt: ['Anneau', 'Cercle']},
	{name: 'PIC_3-3-1', cat: 3, lin: 3, col: 1, txt: ['Rond']},
	{name: 'PIC_3-4-0', cat: 3, lin: 4, col: 0, txt: ['Triangle']},
	{name: 'PIC_3-4-1', cat: 3, lin: 4, col: 1, txt: ['étoile']},
	{name: 'PIC_3-5-0', cat: 3, lin: 5, col: 0, txt: ['Rectangle, Carré']},
	{name: 'PIC_3-5-1', cat: 3, lin: 5, col: 1, txt: ['Plat']},
	{name: 'PIC_3-6-0', cat: 3, lin: 6, col: 0, txt: ['Cube, Pavé']},
	{name: 'PIC_3-6-1', cat: 3, lin: 6, col: 1, txt: ['Sphére']},
	{name: 'PIC_3-7-0', cat: 3, lin: 7, col: 0, txt: ['Pyramide']},
	{name: 'PIC_3-7-1', cat: 3, lin: 7, col: 1, txt: ['Cylindre']},
	{name: 'PIC_3-8-0', cat: 3, lin: 8, col: 0, txt: ['Céne']},
	{name: 'PIC_3-8-1', cat: 3, lin: 8, col: 1, txt: ['Creux', 'Trou', 'Percé']},
	{name: 'PIC_3-9-0', cat: 3, lin: 9, col: 0, txt: ['Grand', 'Plus grand', 'Plus haut']},
	{name: 'PIC_3-9-1', cat: 3, lin: 9, col: 1,  txt: ['Petit', 'Plus petit', 'Plus bas']},
	{name: 'PIC_3-10-0', cat: 3, lin: 10, col: 0, txt: ['Gros', 'Large', 'Plus large/long']},
	{name: 'PIC_3-10-1', cat: 3, lin: 10, col: 1, txt: ['Mince', 'Fin', 'Plus étroit/court']},
	{name: 'PIC_3-11-0', cat: 3, lin: 11, col: 0, txt: ['Haut', 'Monter']},
	{name: 'PIC_3-11-1', cat: 3, lin: 11, col: 1, txt: ['Bas', 'Descendre']},
	{name: 'PIC_3-12-0', cat: 3, lin: 12, col: 0, txt: ['Gauche', 'Début', 'Avant']},
	{name: 'PIC_3-12-1', cat: 3, lin: 12, col: 1, txt: ['Droite', 'Fin', 'Aprés']},
	{name: 'PIC_3-13-0', cat: 3, lin: 13, col: 0, txt: ['Tourner', 'Entourer', 'Cycle']},
	{name: 'PIC_4-0-0', cat: 4, lin: 0, col: 0, txt: ['Rouge']},
	{name: 'PIC_4-0-1', cat: 4, lin: 0, col: 1, txt: ['Orange']},
	{name: 'PIC_4-1-0', cat: 4, lin: 1, col: 0, txt: ['Jaune']},
	{name: 'PIC_4-1-1', cat: 4, lin: 1, col: 1, txt: ['Vert']},
	{name: 'PIC_4-2-0', cat: 4, lin: 2, col: 0, txt: ['Bleu']},
	{name: 'PIC_4-2-1', cat: 4, lin: 2, col: 1, txt: ['Mauve']},
	{name: 'PIC_4-3-0', cat: 4, lin: 3, col: 0, txt: ['Rose']},
	{name: 'PIC_4-3-1', cat: 4, lin: 3, col: 1, txt: ['Brun']},
	{name: 'PIC_4-4-0', cat: 4, lin: 4, col: 0, txt: ['Noir']},
	{name: 'PIC_4-4-1', cat: 4, lin: 4, col: 1, txt: ['Gris']},
	{name: 'PIC_4-5-0', cat: 4, lin: 5, col: 0, txt: ['Blanc']},
	{name: 'PIC_4-5-1', cat: 4, lin: 5, col: 1, txt: ['Transparent', 'Invisible', 'Verre']}
]