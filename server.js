const express = require('express')
const path = require('path')
const app = express()
const board = require('./server/board').default
const cards = require('./server/cards').default
const compression = require('compression')
const cors = require('cors')
const server = require('http').createServer(app);
const io = require('socket.io')(server)

app.use(cors())
app.use(express.json())
app.use(express.urlencoded({ extended: false }))
app.use(compression())

app.set('state', {
	concepts: {green: [], blue: [], red: [], yellow: [], grey: []},
  headers: {green: null, blue: null, red: null, yellow: null, grey: null}
})

app.set('nbCards', 0)
app.use(express.static(path.join(__dirname, 'build')))

app.get('/board', function(req, res) {
	console.log('GET /board')
	res.json(board)
})

app.get('/cards', function(req, res) {
	console.log('GET /cards')
	const i = Math.floor(Math.random() * (cards.length - 5))
	res.json(cards.slice(i, i+5))
})

const getApiAndEmit = async socket => {
	console.log('getApiAndEmit')
  socket.emit("FromAPI", "coucou")
}

io.on("connection", socket => {
  console.log("New client connected")
  socket.emit('state', app.get('state'))
  socket.on('update', data => {
  	app.set('state', data)
  	socket.broadcast.emit('state', data)
  })
  socket.on("disconnect", () => console.log("Client disconnected"))
})

app.get('/', function(req, res) {
  res.sendFile(path.join(__dirname, 'build', 'index.html'))
})


// app.get('/*', function(req, res) {
//   res.sendStatus(404)
// })


const PORT = process.env.PORT || 80
server.listen(PORT, () => {
	console.log(`server running on port ${PORT}`)
})