
# Concept

## dev mode

```
npm install
npm run dev-server
npm run start
```

* server on http://localhost:9000 (nodemon)
* client on http://localhost:3000

## prod mode

```
npm install
npm run build
node server.js
```

* http://localhost:3000

## prod with docker

```
docker build -t concept:1.0 .
docker run --name concept-web -p 8080:3000 concept:1.0
```

## prod with heroku

```
heroku container:login
heroku container create annoa-concept
heroku container:push annoa-concept
heroku container:release annoa-concept
heroku ps:scale annoa-concept=1
```

* /!\ Horoku use env variable $PORT
* on 

