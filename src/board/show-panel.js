import React from 'react'
import { makeStyles, withStyles } from '@material-ui/core/styles'
import Paper from '@material-ui/core/Paper'
import Concept from './concept'
import equal from 'fast-deep-equal'

const useStyles = makeStyles(theme => ({
  header: {
  	padding: 5,
    textAlign: 'center',
    color: theme.palette.text.secondary,
    display: "flex",
    flexDirection: "column",
    justifyContent: "center",
    width: 100,
    height: 140,
    alignItems: 'baseline',
    position:'relative'
  }
}))

const styles = theme => ({
  panel: {
    padding: 3,
    textAlign: 'center',
    color: theme.palette.text.secondary,
    display: 'flex',
    flexWrap: 'wrap',
    '& > *': {
      margin: theme.spacing(1),
      // width: theme.spacing(16),
      // height: theme.spacing(16),
    },
    // "&:hover": {
    //   backgroundColor: 'rgb(0, 0, 0, 0.2)'
    // }
  }
})


function ShowPanelHeader({color, header, handleDrag, updateCubes}) {
	const classes = useStyles()
  const src = `${process.env.PUBLIC_URL}/img/headers/${color}.png`

  return (
  	<React.Fragment>
    <Paper className={classes.header} style={{backgroundColor: color}}>
  		{
  			header &&
  			<Concept 
	      	key={'concept'+header.id} 
	      	data={header}
	      	cubes={header.cubes}
	      	color={color}
	      	updateCubes={updateCubes}
	      	handleDrag={handleDrag}/>
  		}

		<img 
  		width={50}
  		alt={color}
  		unselectable='on'
  		draggable='false'
  		onDragStart={() => false}
  		src={src}
  		style={{position:'absolute', zIndex: 999, top:50, left:-24}} />
    </Paper>
    </React.Fragment>
  )
}


class ShowPanel extends React.Component {

	constructor(props) {
		super(props)
		this.ref = React.createRef()
		window.resizeFuncs[props.color] = this.computeHitbox
	}

	componentDidMount() {
		this.computeHitbox()
	}

	computeHitbox = () => {
		let box = this.ref.current.getBoundingClientRect()
		const {setHitbox, color} = this.props 
		setHitbox(color, {
	    top: box.top + window.pageYOffset,
	    left: box.left + window.pageXOffset,
	    width: box.width,
	    height: box.height
	  })
	}

	handleDrag = (e, data) => {
		const {color, handleDrag} = this.props
		const id = parseInt(data.node.getAttribute('conceptid'))
		const ret = handleDrag(color, id, e.pageX, e.pageY)
		return ret
	}

	//todo : concepts = children ?
	render() {
		const {color, classes, updateCubes, header, concepts} = this.props
	  return (
	  	<div ref={this.ref}>
		    <Paper className={classes.panel} >
		    	<ShowPanelHeader 
		    		color={color} 
		    		handleDrag={this.handleDrag}
		    		updateCubes={updateCubes}
		    		header={header}/>
		      {
		      	concepts.map(c => <Concept 
			      	key={'concept'+c.id} 
			      	data={c}
			      	cubes={c.cubes}
			      	color={color}
			      	handleDrag={this.handleDrag}
			      	updateCubes={updateCubes}/>
			      )
		    	} 
		    </Paper>
	    </div>
	  )
	}
}

export default withStyles(styles, { withTheme: true })(ShowPanel)