import React from 'react'
import { makeStyles, withStyles } from '@material-ui/core/styles'
import Paper from '@material-ui/core/Paper'
import Typography from '@material-ui/core/Typography'
import Draggable from 'react-draggable'
import ButtonGroup from '@material-ui/core/ButtonGroup'
import Button from '@material-ui/core/Button'
import AddIcon from '@material-ui/icons/Add'
import RemoveIcon from '@material-ui/icons/Remove'
import Badge from '@material-ui/core/Badge'

import './concept.css'


const useStyles = makeStyles(theme => ({
  paper: {
    padding: 3,
    textAlign: 'center',
    color: theme.palette.text.secondary,
    cursor: 'grab',
    overflow: 'visible',
    whiteSpace: 'nowrap',
    width: 90,
    height: 130,
    zIndex: 100
  }
}))

const styles = theme => ({
  customBadge: {
    backgroundColor: props => props.color,
    // color: 'rgba(0, 0, 0, 0.54)'
    color: props => props.txtColor
  }
})


const StyledBadge = withStyles(styles)(({ count, classes, children }) => {
  return (
    <div>
      <Badge
        classes={{ badge: classes.customBadge }}
        badgeContent={count}
      >
        {children}
      </Badge>
    </div>
  )
})

export default function Concept({handleDrag, data, onStart, color, cubes, updateCubes}) {
	const classes = useStyles()
	const [count, setCount] = React.useState(cubes || 1)
	const [elevation, setElevation] = React.useState(2)
	const {id, name, txt, cat, lin, col} = data
	const txtColor = (color === 'yellow') ? 'rgba(0, 0, 0, 0.54)': 'white'
	const src = `${process.env.PUBLIC_URL}/img/concepts/${name}.png`

	React.useEffect(() => {
    setCount(cubes);
  }, [cubes])

  const onMouseOver = () => setElevation(8)
	const onMouseOut = () => setElevation(2)


	const renderImg = (src) => <img 
  		height={60} 
  		width={60}
  		alt={txt[0]}
  		unselectable='on'
  		draggable='false'
  		onDragStart={() => false}
  		src={src} />

  return (
	  	<Draggable position={{x: 0, y: 0}} onStop={handleDrag} onStart={onStart}>
	  	
		    <Paper 
		    	conceptid={id} 
		    	className={classes.paper} 
		    	name={name}
					txt={txt}
					cat={cat}
					lin={lin}
					col={col}
					cubes={count}
					style={{width: 90}} 
					elevation={elevation}
					onMouseOver={onMouseOver}
   				onMouseOut={onMouseOut}
		    	>

		    	<div style={{position:'relative'}}>

		    	{ color ?
		    		<React.Fragment>
			    	<StyledBadge color={color} txtColor={txtColor} count={count}>
				    	{renderImg(src)}
			    	</StyledBadge>
			    	<div className='conceptButtonsContainer'>
			      	<ButtonGroup variant="contained" color="primary" size="small">
			          <Button
			          	style={{backgroundColor:color, color: txtColor}}
			            aria-label="reduce"
			            onClick={() => {
			            	const nb = Math.max(count - 1, 0)
			            	updateCubes(color, id, nb)
			              setCount(nb)
			            }}
			          >
			            <RemoveIcon fontSize="small" />
			          </Button>
			          <Button
			          	style={{backgroundColor:color, color: txtColor}}
			            aria-label="increase"
			            onClick={() => {
			            	const nb = count + 1
			            	updateCubes(color, id, nb)
			              setCount(nb)
			            }}
			          >
			            <AddIcon fontSize="small" />
			          </Button>
			        </ButtonGroup>
			      </div>
			    	</React.Fragment>
		    	:
		    		renderImg(src)
		    	}

			    </div>

		      <Typography className='conceptCaption' variant="caption" display="block">
		      	{txt.reduce((all, cur, i) => [...all, <br key={i}/>, cur])}
		      </Typography>
		    </Paper>

		  </Draggable>
  )
}