import React from 'react'
import { makeStyles, withStyles } from '@material-ui/core/styles'
import Paper from '@material-ui/core/Paper'
import Concept from './concept'
import './pick-panel.css'

const styles = theme => ({
  panel: {
    padding: theme.spacing(2),
    textAlign: 'center',
    color: theme.palette.text.secondary,
    display: 'flex',
    position: 'relative',
    flexWrap: 'wrap'
  }
})

const useStyles = makeStyles(theme => ({
  category: {
    flexGrow: 1,
    flexShrink: 0,
    margin: '0 5px',
    backgroundColor: '#ccc',
    overflowY: 'scroll',
    overflowX: 'hidden',
    height: 144
  },
  categoryLine: {
    display: 'flex',
    justifyContent: 'space-evenly',
    '& > *': {
      margin: theme.spacing(1),
      height: theme.spacing(16),
      width: '50%'
    }
  }
}))

function Category({concepts, pickNewConcept, panelRef}) {
  const classes = useStyles()
  const conceptsByLine = []
  concepts.forEach(c => {
    if (!conceptsByLine[c.lin])
      conceptsByLine[c.lin] = []
    conceptsByLine[c.lin].push(c)
  })

  const handleDrag = (e, data) => {
    data.node.style.position = 'static'
    const concept = {
      name: data.node.getAttribute('name'),
      cat: data.node.getAttribute('cat'),
      lin: data.node.getAttribute('lin'),
      col: data.node.getAttribute('col'),
      txt: data.node.getAttribute('txt').split(',')
    }
    return pickNewConcept(concept, e.pageX, e.pageY)
  }

  const onStart = (e, data) => {
    data.node.style.position = 'absolute'
    const panelRect = panelRef.current.getBoundingClientRect()
    const deltaLeft = e.clientX - panelRect.left - 45
    const deltaTop = e.clientY - panelRect.top - 65
    data.node.style.top = deltaTop+'px'
    data.node.style.left = deltaLeft+'px'
  }

  return (
    <div className={classes.category}>
      { 
        conceptsByLine.map( (array,i) => 
          <div key={'categoryLine'+i} className={classes.categoryLine}>
            <Concept data={array[0]} onStart={onStart} handleDrag={handleDrag}/>
            { array.length>1 &&
            <Concept data={array[1]} onStart={onStart} handleDrag={handleDrag}/>
            }
          </div>
        )
      }
    </div>
  )
}

class PickPanel extends React.Component {
  constructor(props) {
    super(props)
    this.ref = React.createRef()
  }

  shouldComponentUpdate(nextProps, nextState) {
    if (!this.props.conceptsRef)
      return false
    return true
  }

  render() {
    const {conceptsRef, pickNewConcept, classes} = this.props
    const conceptsByCat = [[],[],[],[],[]]
    conceptsRef.forEach(concept => {
      conceptsByCat[concept.cat].push(concept)
    })
    return (
      <Paper ref={this.ref} className={classes.panel}>
        <Category panelRef={this.ref} concepts={conceptsByCat[0]} pickNewConcept={pickNewConcept}/>
        <Category panelRef={this.ref} concepts={conceptsByCat[1]} pickNewConcept={pickNewConcept}/>
        <Category panelRef={this.ref} concepts={conceptsByCat[2]} pickNewConcept={pickNewConcept}/>
        <Category panelRef={this.ref} concepts={conceptsByCat[3]} pickNewConcept={pickNewConcept}/>
        <Category panelRef={this.ref} concepts={conceptsByCat[4]} pickNewConcept={pickNewConcept}/>
      </Paper>
    )
  }
}


export default withStyles(styles, { withTheme: true })(PickPanel)