import React from 'react'
import Header from './header.js'
import Grid from '@material-ui/core/Grid'
import PickPanel from './board/pick-panel'
import ShowPanel from './board/show-panel'
import Container from '@material-ui/core/Container'
import socketIOClient from 'socket.io-client'
import axios from 'axios'

const localFetch = axios.create({baseURL: process.env.REACT_APP_BACKEND || ''})

window.resizeFuncs = {
  green: () => {},
  blue: () => {},
  red: () => {},
  yellow: () => {},
  grey: () => {}
}

window.onresize = function() {
  for (let color in window.resizeFuncs)
    (window.resizeFuncs[color])()
}

window.onload = function() {
  for (let color in window.resizeFuncs)
    (window.resizeFuncs[color])()
}

const HEADER_WIDTH = 135

function isInPanel(mouseX, mouseY, hitbox) {
  if (mouseX < hitbox.left)
    return false
  if (mouseY < hitbox.top)
    return false
  if (mouseX > (hitbox.left+hitbox.width))
    return false
  if (mouseY > (hitbox.top+hitbox.height))
    return false
  return true
}

const getInitialState = () => ({
  concepts: {
    green: [],
    blue: [],
    red: [],
    yellow: [],
    grey: []
  },
  headers: {
    green: null,
    blue: null,
    red: null,
    yellow: null,
    grey: null
  }
})

//todo => pick, dragable zone
export default class App extends React.Component {
  state = {
    conceptsRef: [], 
    hitboxes: {
      green: {top:null, left:null, width:null, height: null},
      blue: {top:null, left:null, width:null, height: null},
      red: {top:null, left:null, width:null, height: null},
      yellow: {top:null, left:null, width:null, height: null},
      grey: {top:null, left:null, width:null, height: null},
    },
    ...getInitialState()
  }

  componentDidMount() {
    localFetch.get('board').then(res => {
      this.setState({conceptsRef: res.data})
    }).catch(err => {
      console.log(err)
    })
    this.socket = socketIOClient(process.env.REACT_APP_BACKEND)
    this.socket.on("state", data => {
      this.setState({
        headers: data.headers,
        concepts: data.concepts
      })
      // this.forceUpdate()
    })
  }

  setHitbox = (key, value) => {
    this.setState(state => {
      state.hitboxes[key] = value
      return {hitboxes: state.hitboxes} 
    })
  }

  removeConcept(originColor, id) {
    const {concepts, headers} = this.state
    const n = concepts[originColor].findIndex(c => c.id === id)
    if (n === -1) {
      const concept = {...headers[originColor]}
      headers[originColor] = null
      return concept
    }
    return concepts[originColor].splice(n, 1)[0]
  }

  emitUpdate = () => {
    const boardState = {
      headers: this.state.headers,
      concepts: this.state.concepts
    }
    this.socket.emit('update', boardState)
  }

  isSamePlace(id, color, originColor, isHeader) {
    const {headers, concepts} = this.state
    if (color !== originColor)
      return false
    if (isHeader)
      return (id === headers[originColor].id)
    return (concepts[originColor].findIndex(c => c.id === id) !== -1)
  }

  handleDrag = (originColor, id, pageX, pageY) => {
    const {hitboxes, concepts, headers} = this.state
    
    for (let color in hitboxes) {
      if (isInPanel(pageX, pageY, hitboxes[color])) {
        // in header 
        if (pageX <= hitboxes[color].left + HEADER_WIDTH) {
          if (color === originColor && headers[color] !== null)
            return true
          const concept = this.removeConcept(originColor, id) 
          headers[color] = concept
          this.setState({headers, concepts}, this.emitUpdate)
          return true
        }
        // in panel 
        else {
          if (this.isSamePlace(id, color, originColor, false))
            return true
          const concept = this.removeConcept(originColor, id) 
          concepts[color].push(concept)
          this.setState({headers, concepts}, this.emitUpdate)
        }
        return true
      }
    }
    // outside
    this.removeConcept(originColor, id) 
    this.setState({concepts}, this.emitUpdate)
    return true
  }

  updateCubes = (color, id, nb) => {
    const {concepts, headers} = this.state
    const n = concepts[color].findIndex(c => c.id === id)
    if (n === -1) {
      const concept = headers[color]
      concept.cubes = nb
      this.setState({headers}, this.emitUpdate)
    }
    else {
      const concept = concepts[color][n]
      concept.cubes = nb
      this.setState({concepts}, this.emitUpdate)
    }
  }

  pickNewConcept = (concept, pageX, pageY) => {
    const {hitboxes, concepts, headers} = this.state
    for (let color in hitboxes) {
      if (isInPanel(pageX, pageY, hitboxes[color])) {
        // in header 
        if (pageX <= hitboxes[color].left + HEADER_WIDTH) {
          console.log('In header !')
          concept.id =  Math.floor(Math.random() * 1000000)
          concept.cubes = 1
          headers[color] = concept
          this.setState({headers}, this.emitUpdate)
          return true
        }
        // in panel 
        else {
          concept.id =  Math.floor(Math.random() * 1000000)
          concept.cubes = 1
          concepts[color].push(concept)
          this.setState({concepts}, this.emitUpdate)
          return true
        }
      }
    }
    return true
  }

  onClear = () => {
    this.setState(getInitialState(), this.emitUpdate)
  }

  render() {
    const { concepts, conceptsRef, headers } = this.state
    return (
      <React.Fragment>
        <Header onClear={this.onClear}/>
        <Container style={{padding: 24}}>
          <Grid container spacing={3}>
            <Grid item xs={12}>
              <PickPanel 
                conceptsRef={conceptsRef}
                setHitbox={this.setHitbox} 
                pickNewConcept={this.pickNewConcept}
                />
            </Grid>
            <Grid item xs={12}>
              <ShowPanel main
                color='green'
                header={headers.green}
                concepts={concepts.green}
                setHitbox={this.setHitbox} 
                handleDrag={this.handleDrag}
                updateCubes={this.updateCubes}
                />
            </Grid>
            <Grid item xs={12} sm={6}>
              <ShowPanel
                color='blue' 
                header={headers.blue}
                concepts={concepts.blue}
                setHitbox={this.setHitbox} 
                handleDrag={this.handleDrag}
                updateCubes={this.updateCubes}
                />
            </Grid>
            <Grid item xs={12} sm={6}>
              <ShowPanel
                color='yellow' 
                header={headers.yellow}
                concepts={concepts.yellow}
                setHitbox={this.setHitbox} 
                handleDrag={this.handleDrag}
                updateCubes={this.updateCubes}
                />
            </Grid>
            <Grid item xs={12} sm={6}>
              <ShowPanel
                color='red' 
                header={headers.red}
                concepts={concepts.red}
                setHitbox={this.setHitbox} 
                handleDrag={this.handleDrag}
                updateCubes={this.updateCubes}
                />
            </Grid>
            <Grid item xs={12} sm={6}>
              <ShowPanel
                color='grey' 
                header={headers.grey}
                concepts={concepts.grey}
                setHitbox={this.setHitbox} 
                handleDrag={this.handleDrag}
                updateCubes={this.updateCubes}
                />
            </Grid>
          </Grid>
        </Container>
      </React.Fragment>
    )
  }
}