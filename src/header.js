import React from 'react'
import AppBar from '@material-ui/core/AppBar'
import Toolbar from '@material-ui/core/Toolbar'
import Typography from '@material-ui/core/Typography'
import Button from '@material-ui/core/Button'
import ClearIcon from '@material-ui/icons/Clear'
import LiveHelpIcon from '@material-ui/icons/LiveHelp'
import Dialog from '@material-ui/core/Dialog'
import DialogTitle from '@material-ui/core/DialogTitle'
import DialogContent from '@material-ui/core/DialogContent'
import DialogContentText from '@material-ui/core/DialogContentText'
import Slide from '@material-ui/core/Slide'
import Avatar from '@material-ui/core/Avatar'
import List from '@material-ui/core/List'
import ListItem from '@material-ui/core/ListItem'
import ListItemAvatar from '@material-ui/core/ListItemAvatar'
import ListItemText from '@material-ui/core/ListItemText'
import axios from 'axios'
import './header.css'

const localFetch = axios.create({baseURL: process.env.REACT_APP_BACKEND || ''})

const Transition = React.forwardRef(function Transition(props, ref) {
  return <Slide direction="down" ref={ref} {...props} />;
})

export default function Header({onClear}) {
	const [pickedCard, setPickedCard] = React.useState('')
	const [open, setOpen] = React.useState(false)
	const [cards, setCards] = React.useState([])

  const handleClickOpen = () => {
  	localFetch.get('/cards').then(res => {
  		setCards(res.data)
			setOpen(true)
		}).catch(err => {
      console.log(err)
    })
  }

  const handleClose = () => {
  	setOpen(false)
  }

	const pick = (card) => {
		setPickedCard(card)
		handleClose()
	}

	const clearAll = () => {
		setPickedCard('')
		onClear()
	}

  return (
  	<AppBar position="static">
    	<Toolbar id="header" variant="dense">
	      <Typography variant="h5" style={{flexGrow: 1}}>
	        Concept
	      </Typography>
	      <Typography variant="body1" style={{flexGrow: 0}}>
	        {pickedCard}
	      </Typography>
	      <Button 
		      variant="contained" 
		      startIcon={<LiveHelpIcon/>} 
		      onClick={handleClickOpen}>
	      	Piocher une carte
	      </Button>
	      <Button 
	      	variant="contained" 
	      	startIcon={<ClearIcon/>} 
	      	color="secondary" 
	      	onClick={clearAll}>
	      	Effacer le plateau
	      </Button>
	    </Toolbar>
	    <Dialog
	    	// style={{textAlign: center}}
        open={open}
        TransitionComponent={Transition}
        keepMounted
        onClose={handleClose}
        aria-labelledby="alert-dialog-slide-title"
        aria-describedby="alert-dialog-slide-description"
      >
        <DialogTitle id="alert-dialog-slide-title">Choisissez un mot à faire deviner :)</DialogTitle>
        <List>
        {cards.map(card => (
          <ListItem button onClick={() => pick(card)} key={card}>
            <ListItemAvatar>
              <Avatar variant="rounded">?</Avatar>
            </ListItemAvatar>
            <ListItemText primary={card} />
          </ListItem>
        ))}
	      </List>
	    </Dialog>
	  </AppBar>
  )
}
