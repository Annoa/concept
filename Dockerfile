FROM node:12.2.0-alpine

RUN mkdir -p /usr/src/app
WORKDIR /usr/src/app

COPY . .

RUN npm i
RUN npm run build
# RUN rm -rf node_modules
# RUN npm i --production

# Install sources
# COPY server.js server.js

# Launch command
RUN adduser -D myuser
USER myuser

CMD node server.js